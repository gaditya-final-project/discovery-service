package com.gdaditya.discoveryservice;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DiscoveryServiceApplicationTest {
    @Test
    void contextLoads() {
        DiscoveryServiceApplication.main(new String[]{});
    }

}
